package degirmen.os.cse.gtu.concentrationgame;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Message;
import android.util.Log;
import android.os.Handler;

import java.io.InputStream;
// Reference : http://www.coderzheaven.com/2016/06/10/downloading-multiple-filesimages-etc-in-android-simultaneously-in-android-using-threadpoolexecutor/
public class DownloadThread implements Runnable {

    int threadNo;
    Handler handler;
    String imageUrl;
    Bitmap my_image = null;
    public static final String TAG = "DownloadThread";
    private boolean is_finish = false;

    public DownloadThread() {
    }

    public DownloadThread(int threadNo, String imageUrl, Handler handler) {
        this.threadNo = threadNo;
        this.handler = handler;
        this.imageUrl = imageUrl;
    }

    @Override
    public void run() {
        Log.i(TAG, "Starting Thread : " + Thread.currentThread().getName());
        getBitmap(imageUrl);
        sendMessage(threadNo, "Thread Completed");
        Log.i(TAG, "Thread Completed " + Thread.currentThread().getName());
        is_finish = true;
    }


    public void sendMessage(int what, String msg) {
        Message message = handler.obtainMessage(what, msg);
        message.sendToTarget();
    }

    private Bitmap getBitmap(String url) {
        try {
            // Download Image from URL
            InputStream input = new java.net.URL(url).openStream();
            // Decode Bitmap
            my_image = BitmapFactory.decodeStream(input);
            // Do extra processing with the bitmap

        } catch (Exception e) {
            e.printStackTrace();
        }

        return my_image;
    }

    public Bitmap getImage(){
        if(my_image != null)
            return my_image;
        else{
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return my_image;
        }
    }
    public boolean Is_finish(){
        return is_finish;
    }

}