package degirmen.os.cse.gtu.concentrationgame;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.HttpGet;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class MainActivity extends Activity implements  Handler.Callback{
    String url =  "https://pixabay.com/api/?key=5509978-bb34fc398be7ebb4e05bedea8&q=animal&image_type=photo&pretty=true";
    ArrayList<MyImageClass> image_list = null;
    ArrayList<Bitmap> downloaded_list = new ArrayList<>();
    ArrayList<MyButton> button_list = null;
    GridLayout grd_board = null;
    Button btn_start = null;
    int count=0;
    int step_count = 0;
    public static String TAG = "RelaunchMainActivity";
    public static int GAME_INITIAL_SIZE = 4;

    public static int game_size = 0;
    boolean is_get_images = false;
    HashMap<Integer, Integer> size_map = null;
    int selected_id1 = -1;
    int selected_id2 = -1;
    private TextView myTextView = null;
    private TextView myTextViewScore = null;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();
        Log.d("GAME SZ", ""+GAME_INITIAL_SIZE);
        grd_board = (GridLayout) findViewById(R.id.grd_board);
        InitialBoard(game_size);
        btn_start = (Button)findViewById(R.id.btn_start);
        btn_start.setEnabled(true);

    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void download_function(){
        downloaded_list = new ArrayList<>();
        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(game_size*game_size);

        ArrayList<MyImageClass> new_image_list = new ArrayList<>();
        for(int i=0; i<image_list.size();++i) {
            new_image_list.add(image_list.get(i));
            new_image_list.add(image_list.get(i));
        }

        for (int i = 0; i < new_image_list.size(); i++) {
            String imageUrl = new_image_list.get(i).getUrl();
            DownloadThread dt = new DownloadThread(i, imageUrl, new Handler(this));
            executor.execute(dt);
            while(!dt.Is_finish());
            Log.d("====Download===", i+"size"+button_list.size()+" "+button_list.toString());
            Log.d("Download Function", "--- "+i);
            downloaded_list.add(dt.getImage());


        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        shuffleImage();
        for(int i = 0; i < new_image_list.size(); i++){
            button_list.get(i).setImage(downloaded_list.get(i));
            button_list.get(i).close();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void btn_start_onClick(View v){

        count = 0;
        step_count = 0;
        myTextView.setText("Hamle sayisi:\t"+step_count);
        new Thread(new Runnable() {
            public void run() {
                try {
                    getImages(url, game_size*game_size/2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is_get_images = true;

            }
        }).start();
        while(!is_get_images);
        Log.d("Resimleri aldim.", "Resimleri aldim");
        download_function();
        while(downloaded_list.size() != game_size*game_size);
        playGame(game_size);
        v.setEnabled(false);
        v.setVisibility(View.INVISIBLE);


    }

    public String getImages(String str_url, int number_image) throws IOException {
        InputStream inputStream = null;
        String result = "";
        String new_url = str_url;
        new_url += "&per_page="+number_image;
        image_list = new ArrayList<>();
        is_get_images = false;
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(new_url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null){
                BufferedReader reader = null;
                reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line= reader.readLine()) != null){
                    buffer.append(line);

                }
                String finalJson = buffer.toString();
                JSONObject p_json = new JSONObject(finalJson);
                JSONArray json_list = p_json.getJSONArray("hits");
                for(int i=0; i<number_image ;++i){
                    MyImageClass im = new MyImageClass();
                    JSONObject j = new JSONObject();
                    j = json_list.getJSONObject(i);
                    im.setId((int) j.get("id"));
                    im.setUrl((String) j.get("previewURL"));
                    image_list.add(im);
                    Log.d("Read", im.toString());
                }
                is_get_images = true;
                return buffer.toString();
            }
            else {
                result = "Did not work!";
                Log.d("Hata", inputStream.toString());
            }

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }finally {
            inputStream.close();
        }
        is_get_images = true;
        return result;
    }

    private void shuffleImage(){
        long seed = System.nanoTime();
        Collections.shuffle(downloaded_list, new Random(seed));
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void playGame(int board_size){
        for(int i = 0; i<board_size*board_size; ++i){
            button_list.get(i).close();
            final int finalI = i;
            final int finalI1 = i;
            button_list.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    step_count ++;
                    myTextView.setText("Hamle Sayisi:\t"+step_count);
                    myTextViewScore.setText("Puan:\t"+getScore());
                    if(selected_id1 == -1){
                        button_list.get(finalI).open();
                        selected_id1 = button_list.get(finalI).id;
                        button_list.get(finalI).setEnabled(false);
                    }
                    else{
                        button_list.get(finalI1).open();
                        selected_id2 = button_list.get(finalI1).id;
                        if(button_list.get(finalI1).getImage().sameAs(button_list.get(selected_id1).getImage())){
                            Log.d("Success", "Ayni");
                            button_list.get(finalI1).open();
                            button_list.get(selected_id1).open();
                            button_list.get(finalI1).setEnabled(false);
                            button_list.get(selected_id1).setEnabled(false);
                            if(IsCompleted()){
                                Log.d("Completed", "Next Level");
                                selected_id1 = -1;
                                selected_id2 = -1;
                                game_size += 2;
                                btn_start.setText("Sonraki Seviyeye Basla");

                                btn_start.setEnabled(false);
                                btn_start.setVisibility(View.VISIBLE);
                                Log.d("Sonraki Seviyeye Basla", "Basla");
                                btn_start.setEnabled(true);
                                Toast.makeText(v.getContext(), "Puaniniz: "+getScore(), Toast.LENGTH_LONG);
                            }
                        }
                        else{
                            button_list.get(selected_id1).close();
                            button_list.get(selected_id2).close();
                            button_list.get(selected_id1).setEnabled(true);
                            button_list.get(selected_id2).setEnabled(true);
                        }
                        selected_id1 = -1;
                        selected_id2 = -1;
                    }

                }
            });

        }


    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void InitialBoard(int board_size){
        button_list = new ArrayList<>();
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.setMargins(10, 10 ,10, 10);
        grd_board.setColumnCount(board_size);
        grd_board.setRowCount(board_size);
        grd_board.setForegroundGravity(Gravity.CENTER);
        grd_board.removeAllViews();
        for(int i = 0; i<board_size*board_size; ++i){
            MyButton my_btn = new MyButton(this, i);
            Drawable drawable = this.getResources().getDrawable(R.drawable.img_loading);
            my_btn.setBackground(drawable);
            my_btn.setLayoutParams(params);
            grd_board.addView(my_btn, size_map.get(game_size)*3/5,size_map.get(game_size));
            button_list.add(my_btn);
        }


    }

    @Override
    public boolean handleMessage(Message msg) {
        Log.d("Download", msg.toString());
        if(msg.what == game_size*game_size-1){
            Log.d("Tamamlandi", msg.toString());
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setUp(){
        size_map = new HashMap<>();
        size_map.put(4, 380);
        size_map.put(6, 240);
        size_map.put(8, 180);
        size_map.put(10, 150);
        size_map.put(12, 125);
        size_map.put(14, 110);
        size_map.put(16, 95);
        button_list = new ArrayList<>();
        myTextView = (TextView)findViewById(R.id.txt_second);
        myTextViewScore = (TextView)findViewById(R.id.txt_score);
        game_size = GAME_INITIAL_SIZE;
        image_list = new ArrayList<>();
    }


    private boolean IsCompleted(){
        for(int i = 0; i< button_list.size(); ++i){
            if(!button_list.get(i).Is_Open())
                return false;
        }
        return true;
    }

    public double getScore(){
        double result;
        try{
            result = (game_size*game_size*1.0/step_count)*100;
        }catch (Exception es){
            result = (game_size*game_size*1.0/1)*100;
        }
        return round(result, 2);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}





