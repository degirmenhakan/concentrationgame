package degirmen.os.cse.gtu.concentrationgame;

/**
 * Created by ytu-hakan-degirmen on 03/06/2017.
 */

public class MyImageClass {
    private int id;
    private String url;

    public MyImageClass() {
    }

    public MyImageClass(int id, String url) {

        this.id = id;
        this.url = url;
    }

    public MyImageClass(int id) {

        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "["+id+"]: "+url ;
    }
}
