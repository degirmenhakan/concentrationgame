package degirmen.os.cse.gtu.concentrationgame;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by ytu-hakan-degirmen on 04/06/2017.
 */

@SuppressLint("AppCompatCustomView")
public class MyButton extends Button {
    Bitmap btn_img = null;
    boolean is_open = false;
    int id;
    public MyButton(Context context){
        super(context);
        id=1;

    }
    public MyButton(Context context, int _id){
        super(context);
        id= _id;

    }
    public MyButton(Context context, Bitmap _image, int _id) {
        super(context);
        btn_img = _image;
        id = _id;
        setId(_id);
        setGravity(Gravity.CENTER);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void close(){
        Drawable drawable = this.getResources().getDrawable(R.drawable.btn_cover);
        setBackground(drawable);
        Log.d("Open", "Cart closed");
        is_open = false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void open(){
        BitmapDrawable bdrawable = new BitmapDrawable(this.getResources(), btn_img);
        setBackground(bdrawable);
        Log.d("Open", "Cart opened");
        is_open = true;
    }

    public Bitmap getImage(){
        return btn_img;
    }

    public void setImage(Bitmap _btn_img){
        btn_img = _btn_img;
    }

    public boolean Is_Open(){
        return is_open;
    }

}
